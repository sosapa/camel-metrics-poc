@Grab('org.codehaus.groovy.modules.http-builder:http-builder:0.7')

import groovyx.net.http.RESTClient
//import static groovyx.net.http.ContentType.*
import static groovyx.net.http.ContentType.JSON


RESTClient client = new RESTClient( 'http://localhost:8080/' )
def response = client.get(
  path: 'metrics',
  contentType: JSON
)
assert response.status == 200

println response.getData()

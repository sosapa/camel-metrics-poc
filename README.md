# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Testing apache camel metrics

### Example of test

Run the app
```bash
gw bootRun
```

Generate data
```bash 
 for each in {1..1000}; do
 echo "c1$each" >> fromSource/test${each};
done
```

## Tools
## Wrapper update 

``` 
 gradle wrapper --gradle-version 4.5.1 
```

should result on
```
  gw -version                            
       Picked up JAVA_TOOL_OPTIONS: -Djava.awt.headless=true
       
       ------------------------------------------------------------
       Gradle 4.5.1
       ------------------------------------------------------------
       
       Build time:   2018-02-05 13:22:49 UTC
       Revision:     37007e1c012001ff09973e0bd095139239ecd3b3
       
       Groovy:       2.4.12
       Ant:          Apache Ant(TM) version 1.9.9 compiled on February 2 2017
       JVM:          1.8.0_152 (Azul Systems, Inc. 25.152-b16)
       OS:           Mac OS X 10.13.6 x86_64
       


```
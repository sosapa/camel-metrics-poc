# Docker Setup for metrics

# Docker setup

```bash
docker run -d --name grafana -p 3000:3000 grafana/grafana
docker run -d --name influxdb -p 8086:8086 influxdb


```
## Graphana

## InfluxDB

Reference
[Influx DB](https://hub.docker.com/_/influxdb/)
* Create the DB

```bash
curl  -X POST -d "q=CREATE DATABASE mydb" http://localhost:8086/query
```
* Test write something
```bash
curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'
```

* Graphana setup
** Set access as browser, I suspect the default docker is no right for server to server

* [Springboot 2.0 and influxdb](https://piotrminkowski.wordpress.com/tag/influxdb/) 
* [Springboot 1.5 and influxdb](https://piotrminkowski.wordpress.com/2017/07/13/custom-metrics-visualization-with-grafana-and-influxdb/) 
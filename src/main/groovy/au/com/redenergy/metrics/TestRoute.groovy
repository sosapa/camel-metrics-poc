package au.com.redenergy.metrics


import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.metrics.routepolicy.MetricsRoutePolicyFactory
import org.springframework.stereotype.Component

@Component("testRouteId")
class TestRoute extends RouteBuilder{

  @Override
  void configure() throws Exception {
    log.info("Starting route config")
    context.addRoutePolicyFactory(new MetricsRoutePolicyFactory())

    from("file://./fromSource?delay=500")
        .autoStartup(true)
        .log("bong")
        .to("metrics:timer:simple.timer?action=start")
//        .to("metrics:histogram:simple.histogram?value=9923")
        .log("Yeah file: \${file:name}")
        .routeId("sourceRouteId")
        .to("file://./toTarget")
        .to("metrics:timer:simple.timer?action=stop")
  }
}

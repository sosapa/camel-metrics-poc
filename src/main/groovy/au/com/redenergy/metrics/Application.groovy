// Copyright 2015 Red Energy
// John Hurst (john.hurst@gmail.com)
// 2015-05-21

package au.com.redenergy.metrics

import com.codahale.metrics.MetricRegistry
import groovy.util.logging.Slf4j
import org.apache.camel.CamelContext
import org.apache.camel.component.metrics.MetricsComponent
import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.influxdb.dto.Point
import org.springframework.boot.SpringApplication
import org.springframework.boot.actuate.autoconfigure.ExportMetricWriter
import org.springframework.boot.actuate.endpoint.MetricsEndpoint
import org.springframework.boot.actuate.endpoint.MetricsEndpointMetricReader
import org.springframework.boot.actuate.metrics.Metric
import org.springframework.boot.actuate.metrics.writer.GaugeWriter
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean

import javax.annotation.Resource
import java.util.concurrent.TimeUnit

@Slf4j
@SpringBootApplication
class Application extends SpringBootServletInitializer {

  @Resource
  CamelContext context
  static void main(String[] args) {
    SpringApplication.run(Application, args)
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(Application)
  }

  @Bean
  @ExportMetricWriter
  GaugeWriter influxMetricsWriter() {
    InfluxDB influxDB = InfluxDBFactory.connect("http://localhost:8086", "root", "root")
    String dbName = "mydb"
    influxDB.setDatabase(dbName)
//    influxDB.setRetentionPolicy("one_day")
    influxDB.enableBatch(10, 1000, TimeUnit.MILLISECONDS)

    return new GaugeWriter() {

      @Override
      void set(Metric<?> value) {
        println "** Got value: [$value.name]"
        Point point = Point.measurement(value.getName()).time(value.getTimestamp().getTime(), TimeUnit.MILLISECONDS)
            .addField("value", value.getValue()).build()
        influxDB.write(point)
        log.info("write(" + value.getName() + "): " + value.getValue())
      }
    }
  }

  @Bean
  MetricsEndpointMetricReader metricsEndpointMetricReader(final MetricsEndpoint metricsEndpoint) {
    return new MetricsEndpointMetricReader(metricsEndpoint)
  }

  @Bean(name = MetricsComponent.METRIC_REGISTRY_NAME)
  MetricRegistry getMetricRegistry() {
    MetricRegistry registry = new MetricRegistry()
    return registry
  }
//
//  @Configuration
//  void setup() {
//    context.addRoutePolicyFactory(new MetricsRoutePolicyFactory())
//
//  }

}
